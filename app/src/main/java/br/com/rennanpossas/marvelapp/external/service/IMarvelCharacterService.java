package br.com.rennanpossas.marvelapp.external.service;


import br.com.rennanpossas.marvelapp.data.response.ResponseMarvel;
import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface IMarvelCharacterService {

    @GET("public/characters")
    Observable<ResponseMarvel> getCharacters(@Query("offset") int offset, @Query("limit") int limit, @Query("orderBy") String orderBy, @Query("nameStartsWith") String nameStartsWith);
}
