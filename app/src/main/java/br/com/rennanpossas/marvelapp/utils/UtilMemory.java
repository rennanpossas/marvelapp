package br.com.rennanpossas.marvelapp.utils;

import java.util.HashMap;

public class UtilMemory {

    private static HashMap<String, Object> memory;

    private static UtilMemory utilMemory;

    private UtilMemory(){

    }

    public static UtilMemory getInstance(){
        if(utilMemory == null) {
            utilMemory = new UtilMemory();

            if(memory == null)
                memory = new HashMap<>();
        }

        return utilMemory;
    }

    public void addObject(String key, Object value){
        memory.put(key, value);
    }

    public void removeObject(String key){
        memory.remove(key);
    }

    public <T extends Object> T getObject(String key){
        return (T)memory.get(key);
    }
}
