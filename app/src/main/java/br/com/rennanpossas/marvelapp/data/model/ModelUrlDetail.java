package br.com.rennanpossas.marvelapp.data.model;

import com.google.gson.annotations.Expose;

public class ModelUrlDetail extends ModelDetailBase {

    @Expose
    private final String type = null;
    @Expose
    private final String url = null;

    public String getType() {
        return type;
    }

    public String getUrl() {
        return url;
    }
}
