package br.com.rennanpossas.marvelapp.ui.main;

import br.com.rennanpossas.marvelapp.data.response.ResponseMarvel;
import br.com.rennanpossas.marvelapp.external.api.ApiService;
import br.com.rennanpossas.marvelapp.ui.application.PresenterBase;
import io.reactivex.disposables.CompositeDisposable;

public class MainPresenter extends PresenterBase {

    private final ApiService service;
    private final MainView view;
    private CompositeDisposable subscriptions;

    public MainPresenter(ApiService service, MainView view) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeDisposable();
    }

    public void getCharacters(int offset, String nameStartsWith) {

        service.getCharacters(new ApiService.GetCharactersCallback(){
            @Override
            public void onSuccess(ResponseMarvel charactersListResponse) {
                view.getCharactersList(charactersListResponse);
            }

            @Override
            public void onError(Throwable e) {
                view.onFailure("erro");

            }
        }, offset, nameStartsWith);
    }
    public void onStop() {

    }

}
