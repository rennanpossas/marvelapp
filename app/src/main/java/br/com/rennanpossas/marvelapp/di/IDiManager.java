package br.com.rennanpossas.marvelapp.di;

import javax.inject.Singleton;

import br.com.rennanpossas.marvelapp.external.api.ApiModule;
import br.com.rennanpossas.marvelapp.ui.detail.DetailActivity;
import br.com.rennanpossas.marvelapp.ui.main.MainActivity;
import dagger.Component;

@Singleton
@Component(modules = {ApiModule.class})
public interface IDiManager {

        void injectMain(MainActivity activity);
        void injectDetail(DetailActivity activity);
}
