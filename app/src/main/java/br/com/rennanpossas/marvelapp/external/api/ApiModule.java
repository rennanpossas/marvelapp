package br.com.rennanpossas.marvelapp.external.api;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;

import javax.inject.Singleton;

import br.com.rennanpossas.marvelapp.BuildConfig;
import br.com.rennanpossas.marvelapp.external.service.IMarvelCharacterService;
import br.com.rennanpossas.marvelapp.utils.UtilCrypto;
import dagger.Module;
import dagger.Provides;
import okhttp3.Cache;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ApiModule {

    long cacheMaxTime = 0;

    String baseUrl = null;

    File cacheFile;

    HashMap<String, String> queryParameters;

    public ApiModule(long cacheMaxTime, File cacheFile, String baseUrl){
        this.cacheFile = cacheFile;
        this.cacheMaxTime = cacheMaxTime;
        this.baseUrl = baseUrl;

        queryParameters = new HashMap<>();

    }

    @Provides
    @Singleton
    Retrofit provideCall(){
        Cache cache = new Cache(cacheFile, cacheMaxTime);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        HttpUrl originalHttpUrl = original.url();

                        HttpUrl.Builder builder = originalHttpUrl.newBuilder();

                        String timestamp = "1000";

                        builder.addQueryParameter("apikey", BuildConfig.CONFIG_PUBLIC_MARVEL_API_TOKEN);
                        builder.addQueryParameter("ts", timestamp);
                        builder.addQueryParameter("hash", UtilCrypto.md5Digest(timestamp + BuildConfig.CONFIG_PRIVATE_MARVEL_API_TOKEN + BuildConfig.CONFIG_PUBLIC_MARVEL_API_TOKEN));

                        HttpUrl url = builder.build();

                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .header("Cache-Control", String.format("max-age=%d", cacheMaxTime))
                                .url(url)
                                .build();

                        Response response = chain.proceed(request);
                        response.cacheResponse();
                        return response;
                    }
                })
                .cache(cache)

                .build();


        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @Provides
    @Singleton
    public IMarvelCharacterService providesMarvelCharacterService(Retrofit retrofit){
        return retrofit.create(IMarvelCharacterService.class);
    }

    @Provides
    @Singleton
    public ApiService provideService(IMarvelCharacterService marvel){
        return new ApiService(marvel);
    }
}
