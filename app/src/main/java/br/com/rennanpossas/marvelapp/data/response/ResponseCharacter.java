package br.com.rennanpossas.marvelapp.data.response;

import com.google.gson.annotations.Expose;

import java.util.List;

import br.com.rennanpossas.marvelapp.data.model.ModelCharacter;

public class ResponseCharacter extends ResponseBase {

    @Expose
    private final int offset = 0;
    @Expose
    private final long limit = 0;
    @Expose
    private final long total = 0;
    @Expose
    private final int count = 0;
    @Expose
    private final List<ModelCharacter> results = null;

    public int getOffset() {
        return offset;
    }

    public long getLimit() {
        return limit;
    }

    public long getTotal() {
        return total;
    }

    public int getCount() {
        return count;
    }

    public List<ModelCharacter> getResults() {
        return results;
    }
}
