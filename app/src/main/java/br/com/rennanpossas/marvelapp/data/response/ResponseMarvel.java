package br.com.rennanpossas.marvelapp.data.response;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ResponseMarvel extends ResponseBase {

    @SerializedName("data")
    @Expose
    private final ResponseCharacter characters = null;

    public ResponseCharacter getCharacters() {
        return characters;
    }
}
