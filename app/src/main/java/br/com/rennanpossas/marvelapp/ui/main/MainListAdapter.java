package br.com.rennanpossas.marvelapp.ui.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;

import br.com.rennanpossas.marvelapp.R;
import br.com.rennanpossas.marvelapp.data.model.ModelCharacter;

public class MainListAdapter extends RecyclerView.Adapter<MainListAdapter.ViewHolder> {
    private final OnItemClickListener listener;
    private List<ModelCharacter> data;
    private Context context;

    public MainListAdapter(Context context, List<ModelCharacter> data, OnItemClickListener listener) {
        this.data = data;
        this.listener = listener;
        this.context = context;
    }


    @Override
    public MainListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_list_item, null);
        view.setLayoutParams(new RecyclerView.LayoutParams(RecyclerView.LayoutParams.MATCH_PARENT, RecyclerView.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view);
    }


    @Override
    public void onBindViewHolder(MainListAdapter.ViewHolder holder, int position) {
        holder.click(data.get(position),listener);

        holder.charName.setText(data.get(position).getName());

        Glide.with(context)
                .load(data.get(position).getThumbnail().getPath())
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.ic_launcher_background))
                .into(holder.background);

    }


    @Override
    public int getItemCount() {
        return data.size();
    }


    public interface OnItemClickListener {
        void onClick(ModelCharacter character);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView charName;
        ImageView background;

        public ViewHolder(View itemView) {
            super(itemView);
            charName = (TextView) itemView.findViewById(R.id.charName);
            background = (ImageView) itemView.findViewById(R.id.image);

        }


        public void click(final ModelCharacter character, final OnItemClickListener listener) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onClick(character);
                }
            });
        }
    }
}
