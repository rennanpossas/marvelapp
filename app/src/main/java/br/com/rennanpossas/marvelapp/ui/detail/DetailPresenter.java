package br.com.rennanpossas.marvelapp.ui.detail;

import br.com.rennanpossas.marvelapp.external.api.ApiService;
import br.com.rennanpossas.marvelapp.ui.application.PresenterBase;
import io.reactivex.disposables.CompositeDisposable;

public class DetailPresenter extends PresenterBase {

    private final ApiService service;
    private final DetailView view;
    private CompositeDisposable subscriptions;

    public DetailPresenter(ApiService service, DetailView view) {
        this.service = service;
        this.view = view;
        this.subscriptions = new CompositeDisposable();
    }
}
