package br.com.rennanpossas.marvelapp.ui.application;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Singleton;

import br.com.rennanpossas.marvelapp.BuildConfig;
import br.com.rennanpossas.marvelapp.di.DaggerIDiManager;
import br.com.rennanpossas.marvelapp.di.IDiManager;
import br.com.rennanpossas.marvelapp.external.api.ApiModule;
import br.com.rennanpossas.marvelapp.utils.UtilMemory;

public abstract class AppBase extends AppCompatActivity {
    IDiManager diManager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        File cacheFile = new File(getCacheDir(), "responses");
        diManager = DaggerIDiManager.builder().apiModule(new ApiModule(BuildConfig.CONFIG_CACHE_TIME,cacheFile, BuildConfig.CONFIG_BASE_MARVEL_API_URL)).build();

    }

    protected void goToNextIntent(Class classz){
        Intent intent = new Intent(this, classz);
        this.startActivity(intent);
    }

    protected void showErrorMessage(View view, int resourceId, int duration){

        Snackbar.make(view, resourceId, duration).show();
    }

    public IDiManager getDeps() {
        return diManager;
    }
}
