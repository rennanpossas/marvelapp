package br.com.rennanpossas.marvelapp.ui.main;

import br.com.rennanpossas.marvelapp.data.response.ResponseMarvel;

public interface MainView {

    void onFailure(String appErrorMessage);

    void getCharactersList(ResponseMarvel responseMarvel);
}
