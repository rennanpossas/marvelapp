package br.com.rennanpossas.marvelapp.data.model;

import com.google.gson.annotations.Expose;

public abstract class ModelDetailBase extends ModelBase {

    @Expose
    private final String resourceURI = null;
    @Expose
    private final String name = null;

    public String getResourceURI() {
        return resourceURI;
    }

    public String getName() {
        return name;
    }

}
