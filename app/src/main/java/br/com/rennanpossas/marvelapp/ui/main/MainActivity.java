package br.com.rennanpossas.marvelapp.ui.main;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.rennanpossas.marvelapp.R;
import br.com.rennanpossas.marvelapp.data.model.ModelCharacter;
import br.com.rennanpossas.marvelapp.data.response.ResponseMarvel;
import br.com.rennanpossas.marvelapp.external.api.ApiService;
import br.com.rennanpossas.marvelapp.ui.application.AppBase;
import br.com.rennanpossas.marvelapp.ui.custom.EndlessRecyclerOnScrollListener;
import br.com.rennanpossas.marvelapp.ui.detail.DetailActivity;
import br.com.rennanpossas.marvelapp.utils.UtilConstants;
import br.com.rennanpossas.marvelapp.utils.UtilMemory;

public class MainActivity extends AppBase implements MainView, SearchView.OnQueryTextListener, SearchView.OnAttachStateChangeListener {

    @Inject
    public ApiService service;

    private int offset = 0;

    private RecyclerView list;

    private MainPresenter presenter;

    private MainListAdapter adapter;

    public List<ModelCharacter> charactersList;

    private LinearLayoutManager manager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDeps().injectMain(this);

        setContentView(R.layout.activity_main);

        initializeUi();

        initializeData();

        searchCharacter("");

    }

    private void initializeData() {

        charactersList = new ArrayList<ModelCharacter>();

        adapter = new MainListAdapter(getApplicationContext(), charactersList, new MainListAdapter.OnItemClickListener() {
            @Override
            public void onClick(ModelCharacter character) {
                UtilMemory.getInstance().addObject("character", character);
                goToNextIntent(DetailActivity.class);
            }
        });

        EndlessRecyclerOnScrollListener scrollListener = new EndlessRecyclerOnScrollListener(manager) {
            @Override
            public void onLoadMore(int current_page) {
                presenter.getCharacters(current_page * UtilConstants.API_MARVEL_CONFIG_LIMIT, null);
            }
        };

        list.setAdapter(adapter);

        list.addOnScrollListener(scrollListener);

        presenter = new MainPresenter(service, this);


    }

    private void initializeUi() {
        list = (RecyclerView) findViewById(R.id.list);

        manager = new LinearLayoutManager(this);

        list.setLayoutManager(manager);

        Toolbar toolbar = (Toolbar) findViewById(R.id.mainToolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setSubmitButtonEnabled(true);
        searchView.setOnQueryTextListener(this);
        searchView.addOnAttachStateChangeListener(this);

        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onFailure(String appErrorMessage) {
        showErrorMessage(findViewById(R.id.mainLayout), R.string.generic_communication_error, Snackbar.LENGTH_LONG);
    }

    @Override
    public void getCharactersList(ResponseMarvel responseMarvel) {

        offset += UtilConstants.API_MARVEL_CONFIG_LIMIT;

        charactersList.addAll(responseMarvel.getCharacters().getResults());
        adapter.notifyDataSetChanged();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {

        searchCharacter(query);

        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {

        searchCharacter(newText);

        return true;
    }


    @Override
    public void onViewAttachedToWindow(View v) {

    }

    @Override
    public void onViewDetachedFromWindow(View v) {
        searchCharacter("");
    }

    public void searchCharacter(String name){

        offset = 0;

        charactersList.clear();

        String value = (name.length() == 0)?null:name;

        presenter.getCharacters(offset, value);

    }
}
