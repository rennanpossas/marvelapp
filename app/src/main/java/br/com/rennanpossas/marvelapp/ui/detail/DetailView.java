package br.com.rennanpossas.marvelapp.ui.detail;

public interface DetailView {

    void loadCharacterDetail();

}
