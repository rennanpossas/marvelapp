package br.com.rennanpossas.marvelapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelThumbnail extends ModelBase {

    @Expose
    private final String path = null;

    @Expose
    private final String extension = null;

    public String getPath() {
        return path + "." + extension;
    }

    public String getExtension() {
        return extension;
    }
}
