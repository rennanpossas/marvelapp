package br.com.rennanpossas.marvelapp.ui.detail;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import javax.inject.Inject;

import br.com.rennanpossas.marvelapp.R;
import br.com.rennanpossas.marvelapp.data.model.ModelCharacter;
import br.com.rennanpossas.marvelapp.external.api.ApiService;
import br.com.rennanpossas.marvelapp.ui.application.AppBase;
import br.com.rennanpossas.marvelapp.utils.UtilMemory;

public class DetailActivity extends AppBase implements DetailView {

    @Inject
    public ApiService service;

    private RecyclerView list;

    private DetailPresenter presenter;

    TextView charName;
    TextView charDescription;

    ImageView headerImage;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getDeps().injectDetail(this);

        setContentView(R.layout.activity_detail);

        initializeUi();

        initializeData();
    }

    private void initializeData() {

        ModelCharacter character = (ModelCharacter)UtilMemory.getInstance().getObject("character");

        charName.setText(character.getName());

        charDescription.setText(character.getDescription() == "" ? "Description not found" : character.getDescription());

        Glide.with(this.getApplicationContext())
                .load(character.getThumbnail().getPath())
                .apply(new RequestOptions()
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .placeholder(R.drawable.ic_launcher_background))
                .into(headerImage);

    }

    private void initializeUi() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.mainToolBar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(null);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        charName = (TextView)findViewById(R.id.charName);

        headerImage = (ImageView)findViewById(R.id.headerImage);

        charDescription = (TextView)findViewById(R.id.charDescription);


    }


    @Override
    public void loadCharacterDetail() {

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
