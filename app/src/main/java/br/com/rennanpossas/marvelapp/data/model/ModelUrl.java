package br.com.rennanpossas.marvelapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelUrl extends ModelBase {

    @Expose
    private final int available = 0;
    @Expose
    private final String collectionURI = null;

    @SerializedName("items")
    @Expose
    private final List<ModelUrlDetail> urls = null;

}
