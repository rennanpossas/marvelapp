package br.com.rennanpossas.marvelapp.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ModelStorie extends ModelBase {

    @Expose
    private final int available = 0;
    @Expose
    private final String collectionURI = null;
    @Expose
    private final int returned = 0;

    @SerializedName("items")
    @Expose
    private final List<ModelStorieDetail> stories = null;

    public int getAvailable() {
        return available;
    }

    public String getCollectionURI() {
        return collectionURI;
    }

    public int getReturned() {
        return returned;
    }

    public List<ModelStorieDetail> getStories() {
        return stories;
    }
}
