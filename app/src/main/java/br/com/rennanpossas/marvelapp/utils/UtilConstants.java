package br.com.rennanpossas.marvelapp.utils;

public class UtilConstants {

    public static int API_MARVEL_CONFIG_LIMIT = 10;
    public static String API_MARVEL_DEFAULT_ORDER_BY = "name";

}
