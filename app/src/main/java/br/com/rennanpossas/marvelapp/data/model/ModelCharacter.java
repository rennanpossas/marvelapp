package br.com.rennanpossas.marvelapp.data.model;

import com.google.gson.annotations.Expose;

import java.util.Date;

public class ModelCharacter extends ModelBase {

    @Expose
    private final int id = 0;
    @Expose
    private final String name = null;
    @Expose
    private final String description = null;
    @Expose
    private final Date modified = null;
    @Expose
    private final ModelThumbnail thumbnail = null;
    @Expose
    private final String resourceURI = null;
    @Expose
    private final ModelComic comics = null;
    @Expose
    private final ModelSerie series = null;
    @Expose
    private final ModelStorie stories = null;
    @Expose
    private final ModelEvent events = null;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public Date getModified() {
        return modified;
    }

    public ModelThumbnail getThumbnail() {
        return thumbnail;
    }

    public String getResourceURI() {
        return resourceURI;
    }

    public ModelComic getComics() {
        return comics;
    }

    public ModelSerie getSeries() {
        return series;
    }

    public ModelStorie getStories() {
        return stories;
    }

    public ModelEvent getEvents() {
        return events;
    }
}
