package br.com.rennanpossas.marvelapp.utils;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class UtilCrypto {

    public static String md5Digest(String value) {
        String ret = "";

        try {
            MessageDigest message =MessageDigest.getInstance("MD5");

            message.update(value.getBytes(),0,value.length());

            ret = new BigInteger(1, message.digest()).toString(16);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

        return ret;
    }
}
