package br.com.rennanpossas.marvelapp.external.api;


import br.com.rennanpossas.marvelapp.data.response.ResponseMarvel;
import br.com.rennanpossas.marvelapp.external.service.IMarvelCharacterService;
import br.com.rennanpossas.marvelapp.utils.UtilConstants;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;


public class ApiService {

    private IMarvelCharacterService iMarvelApi;

    public ApiService(IMarvelCharacterService service){

        this.iMarvelApi = service;
    }

    public void getCharacters(final GetCharactersCallback callback, int offset, String nameStartsWith) {

        iMarvelApi.getCharacters(offset, UtilConstants.API_MARVEL_CONFIG_LIMIT, UtilConstants.API_MARVEL_DEFAULT_ORDER_BY, nameStartsWith)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<ResponseMarvel>(){
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(ResponseMarvel responseMarvel) {
                        callback.onSuccess(responseMarvel);
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(e);
                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public interface GetCharactersCallback{
        void onSuccess(ResponseMarvel charactersListResponse);

        void onError(Throwable e);
    }

}
