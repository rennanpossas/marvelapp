package br.com.rennanpossas.marvelapp;

import android.test.suitebuilder.annotation.Smoke;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Observable;

import br.com.rennanpossas.marvelapp.data.response.ResponseMarvel;
import br.com.rennanpossas.marvelapp.external.api.ApiService;
import br.com.rennanpossas.marvelapp.external.service.IMarvelCharacterService;
import br.com.rennanpossas.marvelapp.ui.main.MainActivity;
import br.com.rennanpossas.marvelapp.ui.main.MainPresenter;
import br.com.rennanpossas.marvelapp.ui.main.MainView;
import okhttp3.Response;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */

@RunWith(MockitoJUnitRunner.class)
public class ExampleUnitTest {

    @Mock
    MainPresenter presenter;

    @Mock
    MainActivity view;

    @Mock
    ApiService service;

    @Mock
    IMarvelCharacterService marvelService;

    @Before
    public void setUp(){
        MockitoAnnotations.initMocks(this);
        Mockito.when(marvelService.getCharacters(10, 10, "name", "")).thenReturn(io.reactivex.Observable.just(new ResponseMarvel()));

        presenter = new MainPresenter(service, view);
    }

    @Test
    public void testGetchar(){

    }
}